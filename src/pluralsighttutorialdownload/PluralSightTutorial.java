package pluralsighttutorialdownload;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import static java.lang.System.exit;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import static pluralsighttutorialdownload.UI.displayLogContainer;

public class PluralSightTutorial {

    static WebDriver driver;
    static List<WebElement> webLinks;
    static List<String> videoLinks = new ArrayList<>();
    static List<String> videoCategory = new ArrayList<>();
    static List<String> videoTitles = new ArrayList<>();
    static FileUtil File = new FileUtil();
    static LinkedHashMap<String, String> videoTtleAndLinkPair = new LinkedHashMap<>();
    static String courseName;
    static String videoStoringDirectory;
    static String tutorialUrl;
    static boolean isStopExecution = false;
    static int downloadedVideoCount = 0;
    static String username;
    static String password;

    public static void downloadTutorials() throws AWTException, InterruptedException, IOException {

        retriveCredentialsFromConfigFile();
        checkDirectory();
        if (isStopExecution) {
            addToDisplayLog("------------------Downloading Process Stopped------------------");
            return;
        }
        openChromeBrowser("https://app.pluralsight.com/id/");
        if (isStopExecution) {
            addToDisplayLog("------------------Downloading Process Stopped------------------");
            return;
        }
        loginToPluralsight(username, password);
        if (isStopExecution) {
            addToDisplayLog("------------------Downloading Process Stopped------------------");
            return;
        }
        collectLinksFromTutorialUrl(tutorialUrl);
        printVideoLinks();
        
        if (isStopExecution) {
            addToDisplayLog("------------------Downloading Process Stopped------------------");
            return;
        }
        downloadVideos();
        if (isAllVideoDownloaded()) {
            addToDisplayLog("Number of Video: " + videoTitles.size() + "\nVideo Downloaded: " + downloadedVideoCount);
            addToDisplayLog("---------------------- Download Finished ------------------------");
        } else {
            addToDisplayLog((videoTitles.size() - downloadedVideoCount) + " Videos Couldnt Download....!");
            crossCheckDownloadingProcess();
        }
    }

    public static boolean isAllVideoDownloaded() {
        return videoTitles.size() == downloadedVideoCount;
    }

    public static void test() {
        for (int i = 0; i < 100; i++) {
            displayLogContainer.append("test text " + i + "\n\n\n");
        }
    }

    public static void addToDisplayLog(String msg) {
        displayLogContainer.append(msg + "\n\n");
        focusOnCurrentText();
    }

    public static void focusOnCurrentText() {
        int len = displayLogContainer.getDocument().getLength();
        displayLogContainer.setCaretPosition(len);
        displayLogContainer.requestFocusInWindow();
    }

    private static void retriveCredentialsFromConfigFile() {
        List<String> config = new FileUtil().readFromFile("config");
        username = config.get(0);
        password = config.get(1);
    }

    private static void crossCheckDownloadingProcess() throws InterruptedException, IOException {
        addToDisplayLog("--------------------- Cross Checking Process Started -------------------------");
        downloadedVideoCount = 0;
        downloadVideos();
        if (!isAllVideoDownloaded()) {
            addToDisplayLog("----------------------- Cross Checking Finished -----------------------");
            addToDisplayLog(videoTitles.size() - downloadedVideoCount + " Video faield to Download.....!");
            addToDisplayLog("----------------------- Process Finished -----------------------");
        } else {
            addToDisplayLog("Number of Video: " + videoTitles.size() + "\nVideo Downloaded: " + downloadedVideoCount);
            addToDisplayLog("---------------------- Download Finished ------------------------");
        }
    }

    private static void printTutorialName() {
        courseName = driver.findElement(By.xpath("//h1[@class = 'course-hero__title']")).getText();
        courseName = courseName.replaceAll("[*:?<>/|\"]", " -");
        addToDisplayLog("Course Name:  " + courseName + "");
    }

    private static void printTotalNumberOfVideos() {
        driver.findElement(By.linkText("Expand all")).click();
        webLinks = driver.findElements(By.xpath("//a[@class = 'table-of-contents__clip-list-title']"));
        addToDisplayLog("Total Number of Videos: " + webLinks.size() + "");
    }

    public static void displayException(Exception ex) {
        addToDisplayLog("========================= Exception ===========================");
        addToDisplayLog(ex.toString());
        addToDisplayLog("===============================================================");
    }

    private static boolean isVideoFormNewCategory(int i) {
        String url = videoLinks.get(i);
        // below condition determines, if the video link is of a new Module/Category
        return ((url.contains("0&mode=live") && !(url.contains("10&mode=live") 
                || url.contains("20&mode=live") || url.contains("30&mode=live")
                || url.contains("40&mode=live") || url.contains("50&mode=live")
                || url.contains("60&mode=live") || url.contains("70&mode=live")
                || url.contains("80&mode=live") || url.contains("90&mode=live"))));
    }

    public static class FrameRunnable implements Runnable {

        @Override
        public void run() {
            new UI().setVisible(true);
        }
    }

    public static void init(String directory, String Url) {
        videoStoringDirectory = directory;
        tutorialUrl = Url;
    }

    private static void loginToPluralsight(String username, String password) {
        try {
            addToDisplayLog("Signing in to PluralSight....");
            driver.findElement(By.id("Username")).sendKeys(username);
            driver.findElement(By.id("Password")).sendKeys(password);
            driver.findElement(By.id("login")).click();

        } catch (Exception ex) {
            Toolkit.getDefaultToolkit().beep();
            displayException(ex);
            addToDisplayLog("Couldn't Sign in to PluralSight....! Downloading Process Stopped..!");
            isStopExecution = true;
        }
    }

    public static void collectLinksFromTutorialUrl(String url) throws IOException, InterruptedException, AWTException {
        try {
            driver.get(url);
            Thread.sleep(3000);
            if (!driver.getCurrentUrl().equals(url)) {
                Toolkit.getDefaultToolkit().beep();
                addToDisplayLog(" ERROR: Check Username and Password...!");
                isStopExecution = true;
                return;
            }
            isStopExecution = false;
            printTutorialName();
            storeVideoCategories(driver.findElements(By.xpath("//a[@class = '']")));
            printTotalNumberOfVideos();
            storeLinksAndTitles();

        } catch (Exception ex) {
            Toolkit.getDefaultToolkit().beep();
            displayException(ex);
            addToDisplayLog("Starting the Download Process Again......");
            downloadTutorials();
        }
    }

    private static void printVideoLinks() {
        int index = -1;
        addToDisplayLog("======================== Video Links ========================= ");
        for (int i = 0; i < videoLinks.size(); i++) {
            if (isVideoFormNewCategory(i)) {
                ++index;
                addToDisplayLog("------ Category :" + videoCategory.get(index) + "------");
            }
            addToDisplayLog(videoTitles.get(i) + ": " + videoLinks.get(i) + "");
        }
    }

    private static void downloadVideos() throws InterruptedException, IOException {
        String category;
        String videoDownloadingUrl;
        String videoDirectory = null;
        System.out.println("========================== Downloading Videos ========================");
        addToDisplayLog("========================= Downloading Videos =========================");
        int videoCategoryIndex = -1;

        for (int i = 0; i < videoLinks.size(); i++) {
            try {
                driver.get(videoLinks.get(i));
            } catch (Exception ex) {
                displayException(ex);
                openChromeBrowser("https://app.pluralsight.com/id/");
                loginToPluralsight(username, password);
                --i;
                continue;
            }
            if (isVideoFormNewCategory(i)) {
                videoCategoryIndex++;
                category = PluralSightTutorial.videoCategory.get(videoCategoryIndex);
                addToDisplayLog("------ Category :" + category + "------");
                videoDirectory = videoStoringDirectory + "/" + courseName + "/" + category;
                createDirectory(videoDirectory);
            }
            try {
                Thread.sleep(5000);
                videoDownloadingUrl = driver.findElement(By.tagName("video")).getAttribute("src");
                if (videoDownloadingUrl.length() == 0) {
                    --i;
                    continue;
                }
                System.out.println("Video Title: " + videoTitles.get(i) + "\nVideo URL: " + videoDownloadingUrl);
                addToDisplayLog("Video Title: " + videoTitles.get(i) + "\nVideo URL: " + videoDownloadingUrl + "");
                Downloader.downloadVideoFromUrl(videoDownloadingUrl, videoDirectory, videoTitles.get(i));
            } catch (Exception ex) {
                displayException(ex);
                --i;
            }
        }
    }

    private static void storeVideoCategories(List<WebElement> links) {
        int videoCategoryNumber = 1;
        for (int i = 0; i < links.size(); i++) {
            videoCategory.add(videoCategoryNumber + ". " + links.get(i).getText().replaceAll("[*:?<>/|\"]", ""));
            videoCategoryNumber++;
        }
    }

    private static void openChromeBrowser(String url) {
        try {
            addToDisplayLog("Opening Google Chrome Browser....");
            System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver.exe");
            driver = new ChromeDriver();
            driver.get(url);
        } catch (Exception ex) {
            Toolkit.getDefaultToolkit().beep();
            displayException(ex);
            isStopExecution = true;
        }
    }

    private static void createDirectory(String videoDirectory) {
        try {
            new File(videoDirectory).mkdirs();
            //JOptionPane.showMessageDialog(null, "Called");
        } catch (Exception ex) {
            Toolkit.getDefaultToolkit().beep();
            displayException(ex);
            JOptionPane.showMessageDialog(null, "Coudnt Create Directory: " + videoDirectory + "\n Program Will Exit.");
            exit(1);
        }
    }

    private static void storeLinksAndTitles() throws AWTException, InterruptedException, IOException {
        int videoLinksTitlesNumber = 1;
        for (int i = 0; i < webLinks.size(); i++) {
            if (webLinks.get(i).getText().length() <= 0 || webLinks.get(i).getText() == null) {
                addToDisplayLog("ERROR: Couldn't load Video Titles Properly...! Process Restarting....!");
                downloadTutorials();
            }
            videoLinks.add(webLinks.get(i).getAttribute("href"));
            videoTitles.add(videoLinksTitlesNumber + ". " + webLinks.get(i).getText().replaceAll("[*:?<>/|\"]", ""));
            videoLinksTitlesNumber++;
        }
    }
    
    private static boolean pathIsValid(String videoStoringDirectory) {
        return pathStringIsCorrect(videoStoringDirectory) && pathExists(videoStoringDirectory);
    }

    public static boolean pathStringIsCorrect(String path) {
        try {
            Paths.get(path);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
        return true;
    }

    public static boolean pathExists(String path) {
        return Files.exists(Paths.get(path));
    }
    
    private static void checkDirectory() {
        while (true) {
            if ((videoStoringDirectory != null) && (videoStoringDirectory.length() > 0)) {
                if (pathIsValid(videoStoringDirectory)) {
                    addToDisplayLog("Directory Checked.....");
                    isStopExecution = false;
                    return;
                } else {
                    addToDisplayLog("Invalid Directory....! ");
                    isStopExecution = true;
                    JOptionPane.showMessageDialog(null, "Invalid Directory...!");
                    return;
                }
            } else {
                addToDisplayLog("Invalid Directory....! ");
                isStopExecution = true;
                JOptionPane.showMessageDialog(null, "Invalid Directory...!");
                return;
            }
        }
    }
}
