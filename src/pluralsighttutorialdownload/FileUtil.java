/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pluralsighttutorialdownload;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author apl
 */
public class FileUtil {

    public void writeToFile(String fileName, String data) throws IOException {
        try (
                FileWriter fileWritter = new FileWriter(fileName, false);
                BufferedWriter bw = new BufferedWriter(fileWritter)) {
            bw.write(data);
        } catch (Exception ex) {
            Toolkit.getDefaultToolkit().beep();
            PluralSightTutorial.displayException(ex);
        }
    }

    public List<String> readFromFile(String fileName) {
        List<String> config = new ArrayList<>();
        String line;
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                config.add(line);
            }
        } catch (Exception ex) {
            Toolkit.getDefaultToolkit().beep();
            PluralSightTutorial.displayException(ex);
            JOptionPane.showMessageDialog(null, "Config File not Found...!\n\nSave Username & Password in Configuration Tab. Then start the Download Again.");
            PluralSightTutorial.isStopExecution = true;
        }
        return config;
    }
}
